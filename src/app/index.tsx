/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import { useEffect } from 'react';
import { Helmet } from 'react-helmet-async';
import { Switch, Route, BrowserRouter } from 'react-router-dom';

import { GlobalStyle } from 'styles/global-styles';

import { HomePage } from './containers/HomePage/Loadable';
import { NotFoundPage } from './components/NotFoundPage/Loadable';
import { useTranslation } from 'react-i18next';
import AOS from 'aos';
import 'aos/dist/aos.css';

export function App() {
  const { i18n } = useTranslation();

  useEffect(() => {
    AOS.init({
      duration: 3000,
    });
  }, []);

  return (
    <BrowserRouter>
      <Helmet
        titleTemplate="%s - Emoha"
        defaultTitle="Emoha"
        htmlAttributes={{ lang: i18n.language }}
      >
        <meta name="description" content="A Emoha application" />
      </Helmet>

      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route component={NotFoundPage} />
      </Switch>

      <GlobalStyle />
    </BrowserRouter>
  );
}
