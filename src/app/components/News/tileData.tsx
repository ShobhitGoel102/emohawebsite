const tileData = [
  {
    img: 'https://emoha.com/sites/default/files/icon/EMOHA-Team.jpg',
    title: 'Breakfast',
    author: 'jill111',
    cols: 2,
    featured: true,
  },
  {
    img: 'https://emoha.com/sites/default/files/icon/EMOHA-Team.jpg',
    title: 'Tasty burger',
    author: 'director90',
  },
  {
    img: 'https://emoha.com/sites/default/files/icon/EMOHA-Team.jpg',
    title: 'Camera',
    author: 'Danson67',
  },
  {
    img: 'https://emoha.com/sites/default/files/icon/EMOHA-Team.jpg',
    title: 'Morning',
    author: 'fancycrave1',
    featured: true,
  },
  {
    img: 'https://emoha.com/sites/default/files/icon/EMOHA-Team.jpg',
    title: 'Hats',
    author: 'Hans',
  },
];

export default tileData;
