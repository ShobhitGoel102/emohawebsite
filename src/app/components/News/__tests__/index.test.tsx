import * as React from 'react';
import { render } from '@testing-library/react';

import { News } from '..';

describe('<News  />', () => {
  it('should match snapshot', () => {
    const loadingIndicator = render(<News />);
    expect(loadingIndicator.container.firstChild).toMatchSnapshot();
  });
});
