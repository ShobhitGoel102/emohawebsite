import * as React from 'react';
import { render } from '@testing-library/react';

import { Services } from '..';

describe('<Services  />', () => {
  it('should match snapshot', () => {
    const loadingIndicator = render(<Services />);
    expect(loadingIndicator.container.firstChild).toMatchSnapshot();
  });
});
