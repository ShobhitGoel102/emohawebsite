import * as React from 'react';
import { render } from '@testing-library/react';

import { Gallery } from '..';

describe('<Gallery  />', () => {
  it('should match snapshot', () => {
    const loadingIndicator = render(<Gallery />);
    expect(loadingIndicator.container.firstChild).toMatchSnapshot();
  });
});
