/* eslint-disable no-restricted-globals */
/**
 *
 * Gallery
 *
 */
import React, { memo, useEffect } from 'react';
import styled from 'styled-components/macro';
import { gsap } from 'gsap';
import { ScrollTrigger } from 'gsap/all'
import './style.css';

interface Props {}

gsap.registerPlugin(ScrollTrigger);

export const Gallery = memo((props: Props) => {
  useEffect(() => {
    gsap
      .timeline({
        scrollTrigger: {
          trigger: '.grid-container',
          start: 'top top',
          end: () => innerHeight * 4,
          scrub: true,
          pin: '.grid',
          anticipatePin: 1,
        },
      })
      .set('.gridBlock:not(.centerBlock)', { autoAlpha: 0 })
      .to(
        '.gridBlock:not(.centerBlock)',
        { duration: 0.1, autoAlpha: 1 },
        0.001,
      )
      .from('.gridLayer', {
        scale: 3.3333,
        ease: 'none',
      });

    // Images to make it look better, not related to the effect
    const size = Math.max(innerWidth, innerHeight);
    gsap.set('.gridBlock', {
      backgroundImage: i =>
        `url(https://emoha.com/sites/default/files/iStock-115009850_Assure.jpg)`,
    });

    const bigImg = new Image();
    bigImg.addEventListener('load', function () {
      gsap.to('.centerPiece .gridBlock', { autoAlpha: 1, duration: 0.5 });
    });

    bigImg.src = `https://emoha.com/sites/default/files/gva-sliderlayer-upload/1-ehzhjy-pqzsdj.jpg`;
  }, []);
  return (
    <Div>
      <div className="grid-container">
        <div className="grid">
          <div className="gridLayer">
            <div className="gridBlockBorder">
            <div className="gridBlock"></div>
            </div>

          </div>
          <div className="gridLayer">
            <div className="gridBlock"></div>
          </div>
          <div className="gridLayer">
            <div className="gridBlock"></div>
          </div>
          <div className="gridLayer centerPiece">
            <div className="gridBlock centerBlock"></div>
          </div>
          <div className="gridLayer">
            <div className="gridBlock">
              <a href="https://greensock.com" target="_blank"></a>
            </div>
          </div>
          <div className="gridLayer">
            <div className="gridBlock"></div>
          </div>
          <div className="gridLayer">
            <div className="gridBlock"></div>
          </div>
          <div className="gridLayer">
            <div className="gridBlock"></div>
          </div>
          <div className="gridLayer">
            <div className="gridBlock"></div>
          </div>
        </div>
      </div>
    </Div>
  );
});

const Div = styled.div``;
