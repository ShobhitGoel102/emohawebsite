/**
 *
 * Asynchronously loads the component for Gallery
 *
 */

import { lazyLoad } from 'utils/loadable';

export const Gallery = lazyLoad(
  () => import('./index'),
  module => module.Gallery,
);
