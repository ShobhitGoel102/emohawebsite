/**
 *
 * Asynchronously loads the component for EmohaElderCare
 *
 */

import { lazyLoad } from 'utils/loadable';

export const EmohaElderCare = lazyLoad(
  () => import('./index'),
  module => module.EmohaElderCare,
);
