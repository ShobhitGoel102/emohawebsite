/**
 *
 * ElderCare
 *
 */
import React, { memo } from 'react';
import { styled } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';
import CardMedia from '@material-ui/core/CardMedia';
interface Props {}

export const EmohaElderCare = memo((props: Props) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { t, i18n } = useTranslation();
  return (
    <Container fixed>
      <Box
        flexWrap="wrap"
        display="flex"
        css={{ Width: '100%', marginTop: '120vh', marginBottom: '40vh'}}
        p={1}
        justifyContent="space-between"
      >
        <Box
          display="flex"
          flexWrap="wrap"
          p={1}
          m={1}
          css={{ minWidth: 300, maxWidth: '40vw' }}
          data-aos="fade-up"
        >
          <Box display="flex" flexDirection="row">
            <WhiteText variant="h4" gutterBottom>
              Emoha
            </WhiteText>
            <RedText variant="h4" gutterBottom>
              Elder Care
            </RedText>
          </Box>
          <DescText paragraph gutterBottom data-aos="fade-up-right">
            Emoha is a community of elders and those who love them. Members of
            our community benefit from a comprehensive bouquet of curated elder
            care at home, offers, and activities, through an app designed
            exclusively for elders.
          </DescText>
          <LearnMoreLink href="https://" >Learn More</LearnMoreLink>
        </Box>

        <ElderImageBox data-aos="fade-right">
          <ElderImage
            image="https://emoha.com/sites/default/files/gva-sliderlayer-upload/1-ehzhjy-pqzsdj.jpg"
            title="Emoha"
          />
        </ElderImageBox>
      </Box>
    </Container>
  );
});

const WhiteText = styled(Typography)({
  color: '#8b8b8b',
  fontFamily: 'Product Sans',
  marginRight: '10px',
});

const RedText = styled(Typography)({
  color: '#dc3941',
  fontFamily: 'Product Sans',
});

const DescText = styled(Typography)({
  width: '100%',
  fontFamily: 'Product Sans',
});

const LearnMoreLink = styled(Link)({
  color: '#dc3941',
  '&:hover': {
    color: '#dc3941',
  },
  fontFamily: 'Product Sans',
});

const ElderImage = styled(CardMedia)(props => ({
  [props.theme.breakpoints.down('sm')]: {
    height: '200px',
    width: '300px',
  },
  [props.theme.breakpoints.up('md')]: {
    height: '25vh',
    width: '20vw',
  },
  [props.theme.breakpoints.up('lg')]: {
    height: '30vh',
    width: '30vw',
  },
}));

const ElderImageBox = styled(Box)({
  border: '10px solid black',
  padding: '10px',
  boxShadow: '10px 10px 5px #d5d5d5',
});
