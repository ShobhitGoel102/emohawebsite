import * as React from 'react';
import { render } from '@testing-library/react';

import { EmohaElderCare } from '..';

describe('<EmohaElderCare  />', () => {
  it('should match snapshot', () => {
    const loadingIndicator = render(<EmohaElderCare />);
    expect(loadingIndicator.container.firstChild).toMatchSnapshot();
  });
});
