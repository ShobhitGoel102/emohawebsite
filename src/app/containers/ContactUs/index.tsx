/**
 *
 * ContactUs
 *
 */

import React, { memo } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components/macro';

import { useInjectReducer, useInjectSaga } from 'utils/redux-injectors';
import { reducer, sliceKey } from './slice';
import { selectContactUs } from './selectors';
import { contactUsSaga } from './saga';
import { messages } from './messages';

interface Props {}

export const ContactUs = memo((props: Props) => {
  useInjectReducer({ key: sliceKey, reducer: reducer });
  useInjectSaga({ key: sliceKey, saga: contactUsSaga });

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const contactUs = useSelector(selectContactUs);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const dispatch = useDispatch();

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { t, i18n } = useTranslation();

  return (
    <>
      <Div>
        {t('')}
        {/* {t(...messages.someThing)} */}
      </Div>
    </>
  );
});

const Div = styled.div``;
