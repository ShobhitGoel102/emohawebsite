import { PayloadAction } from '@reduxjs/toolkit';
import { createSlice } from 'utils/@reduxjs/toolkit';
import { ContainerState } from './types';

// The initial state of the ContactUs container
export const initialState: ContainerState = {};

const contactUsSlice = createSlice({
  name: 'contactUs',
  initialState,
  reducers: {
    someAction(state, action: PayloadAction<any>) {},
  },
});

export const {
  actions: contactUsActions,
  reducer,
  name: sliceKey,
} = contactUsSlice;
