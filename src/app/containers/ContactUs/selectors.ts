import { createSelector } from '@reduxjs/toolkit';

import { RootState } from 'types';
import { initialState } from './slice';

const selectDomain = (state: RootState) => state.contactUs || initialState;

export const selectContactUs = createSelector(
  [selectDomain],
  contactUsState => contactUsState,
);
