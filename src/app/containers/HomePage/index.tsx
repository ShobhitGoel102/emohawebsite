import * as React from 'react';
import { Helmet } from 'react-helmet-async';
import { Footer } from 'app/components/Footer';
import { EmohaElderCare } from 'app/components/EmohaElderCare';
import { Gallery } from 'app/components/Gallery';
import { Services } from '../../components/Services';
import { News } from 'app/components/News';

export function HomePage() {
  return (
    <>
      <Helmet>
        <title>Emoha</title>
        <meta name="description" content="Emoha" />
      </Helmet>
      <Gallery />
      <EmohaElderCare />
      <Services />
      <News />
      <Footer />
    </>
  );
}
